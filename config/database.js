//import { Sequelize, Model, DataTypes } from 'sequelize'
const { Sequelize, Model, DataTypes } = require('sequelize')
const { DB_USER,DB_HOST,DB_NAME,DB_PASSWORD,DB_PORT } = process.env;

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
    DB_HOST,
    DB_PORT,
    dialect: 'postgres',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
    logging: false
  });

module.exports =  {
    sequelize
};
