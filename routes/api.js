const express = require('express')
const router = express.Router()
const {registerUser,loginUser,welcomeUser} = require("../controllers/api/loginController");
const {verifyToken} = require("../middleware/auth");

// middleware that is specific to this router
router.use((req, res, next) => {
  //console.log('Time: ', Date.now())
  next()
})
/* // define the home page route
router.get('/', (req, res) => {
  res.send('Birds home page')
})
// define the about route
router.get('/about', (req, res) => {
  res.send('About birds')
}) */

router.post("/register",registerUser);
router.post("/login",loginUser);

const auth = verifyToken;
router.post("/welcome", auth, welcomeUser);

module.exports = router