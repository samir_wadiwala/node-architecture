const {registerView, loginView,forgotPassword,registerUser,loginUser,loginOut,forgotPasswordSend} = require("../controllers/loginController");
const {productView,productCreate,productStore,productEdit,productUpdate,productDelete} = require("../controllers/productController");
const {authenticate} = require("../middleware/auth");

module.exports = function(app){

    app.get('/',authenticate, (req, res) => {
        return res.render("index",{ title: 'Dashboard', layout: './layouts/sidebar'});
    });
    app.get('/register', authenticate, registerView);
    app.get('/login',authenticate, loginView);
    app.post('/register', authenticate, registerUser);
    app.post('/login', authenticate, loginUser);
    app.get('/logout', authenticate, loginOut);
    app.get('/forgot-password', authenticate, forgotPassword);
    app.post('/forgot-password', authenticate, forgotPasswordSend);

    //product
    app.get('/product',authenticate, productView);
    app.get('/product-create', authenticate, productCreate);
    app.post('/product-store', authenticate, productStore);
    app.get('/product-edit', authenticate, productEdit);//product-edit/:id
    app.post('/product-update', authenticate, productUpdate);
    app.get('/product-delete', authenticate, productDelete);


}