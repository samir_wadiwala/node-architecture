const jwt = require("jsonwebtoken");

const config = process.env;
// for api 
const verifyToken = (req, res, next) => {
  const token =
    req.body.token || req.query.token || req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    const decoded = jwt.verify(token, config.TOKEN_KEY);
    req.user = decoded;
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};
//for web
const authenticate = (req, res, next) => {
    var url = req.originalUrl;
    var user = req.session.user;
    if(url =="/login" || url =="/register" || url =="login" || url =="register" || url=="/forgot-password" || url=="forgot-password"){
        if(user){
            return res.redirect("/");
        }
    }else if(!user){
        return res.redirect("/login");
    }
    return next();
  };
  
module.exports = {authenticate,verifyToken};