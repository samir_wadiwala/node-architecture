var express = require('express')
const session = require('express-session')
const expressLayouts = require('express-ejs-layouts')
var bodyParser=require("body-parser");
require("dotenv").config();
//const {sequelize} = require("./config/database");
//const db = require("./models");

const app = express();
const { PORT } = process.env;
const { HOSTNAME } = process.env;
//db.sequelize.sync();

const db = require("./models");
db.sequelize.sync();
/* db.sequelize.sync().then((req) => {
    console.log('====================');
    console.log(req.models);
}); */


app.use(express.json());

app.use(bodyParser.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: true
}));
// Session Setup
app.use(session({
  
    // It holds the secret key for session
    secret: 'demo',
  
    // Forces the session to be saved
    // back to the session store
    resave: true,
  
    // Forces a session that is "uninitialized"
    // to be saved to the store
    saveUninitialized: true
}))

app.use('/css',express.static(__dirname +'/resources/css'));
app.use('/vendor',express.static(__dirname +'/resources/vendor'));
app.use('/js',express.static(__dirname +'/resources/js'));
app.use('/img',express.static(__dirname +'/resources/img'));
app.use(expressLayouts)
app.set('layout', './layouts/main')
//View engine setup
app.set('view engine', 'ejs');
app.disable('view cache');

// call route from route file
require('./routes/routes')(app);

const api = require('./routes/api');
app.use('/api', api)

app.listen(PORT, HOSTNAME, () => {
    console.log(`Example app listening at http://${HOSTNAME}:${PORT}`)
})