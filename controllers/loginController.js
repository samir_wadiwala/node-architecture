//const User = require("../models/User");
/* const db = require("../models");
const User = db.User; */
const db = require("../models");
const User = db.users;
//const Op = db.Sequelize.Op;
var bcrypt = require('bcryptjs');
//For Register Page
const registerView = (req, res) => {

    return res.render("login/register",{title:'Register'});
}

// For View 
const loginView = (req, res) => {
    
    //res.send(User.findAll());return false;
    return res.render("login/index",{title: 'Login'});
}

//forgotPassword
const forgotPassword = (req, res) => {
    res.render("login/forgot-password",{title:'Forgot Password'});
}

//For Store Register
const registerUser = async (req, res) => {
    const { firstName, lastName, email, password } = req.body;
    //Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10);
    const user = await User.create({
        firstName,
        lastName,
        email: email.toLowerCase(), // sanitize: convert email to lowercase
        password: encryptedPassword,
    });
    if(user){
        return res.redirect('/login');
    }else{
        return res.redirect('/register');
    }   
}

// login user
const loginUser = async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email } });
    if (user && (await bcrypt.compare(password, user.password))) {
        req.session.user = user.dataValues;
        res.redirect("/");
    }else{
        res.redirect("/login");
    }
}
// logout
const loginOut = (req, res) => {
    req.session.destroy(function (err) {
        res.redirect('login'); //Inside a callback… bulletproof!
    });
}

const forgotPasswordSend = async (req, res) => {
    const { email } = req.body;
    const user = await User.findOne({ where: { email } });
    if (user){
        res.redirect("/login");
    }else{
        res.redirect("/forgot-password");
    }
}

module.exports =  {
    registerView,
    loginView,
    registerUser,
    loginUser,
    loginOut,
    forgotPassword,
    forgotPasswordSend
};