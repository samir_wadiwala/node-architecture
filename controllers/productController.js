const db = require("../models");
const Product = db.products;
//For Product
const productView = async (req, res) => {
    const products = await Product.findAll({});
    return res.render("./product/index",{ title: 'Product',data:products, layout: './layouts/sidebar'});
}

const productCreate = async (req, res) => {

    return res.render("./product/create",{ title: 'Product Create', layout: './layouts/sidebar'});

}

const productStore = async (req, res) => {

    var data = {
        "productNo": req.body.productNo,
        "productName": req.body.productName,
        "status": req.body.status
    }
    const product = await Product.create(data);
    if(product){
        return res.redirect('/product');
    }else{
        return res.redirect('/product-create');
    } 

}

const productEdit = async (req, res) => {
    //var id = req.params.id;
    const id = req.query.id;
    var pro_id = id;
    const product = await Product.findOne({ where:{'id':pro_id}});
    if(product){
        return res.render("./product/edit",{ title: 'Product Edit',data:product, layout: './layouts/sidebar'});
    }else{
        return res.redirect('/product');
    }
}

const productUpdate = async (req, res) => {
    var pro_id = req.body.qId;
    const product= await Product.update({"productNo": req.body.productNo, "productName": req.body.productName, "status": req.body.status }, {
        where: {
            'id':pro_id
        }
      });
    if(product){
        return res.redirect('/product');
    }else{
        return res.redirect('/product-edit?id='+req.body.qId);
    }

}

const productDelete = async (req, res) => {

    const id = req.query.id;
    const product = await Product.destroy({
        where: {
            'id':id
        }
    });
    if(product){
        return res.redirect('/product');
    }else{
        return res.redirect('/product');
    }

}

module.exports =  {
    productView,
    productCreate,
    productStore,
    productEdit,
    productUpdate,
    productDelete
};